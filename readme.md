# Getting started

* The following folder structure is assumed

* US-domestic-airflights      
	- data ( .CSV files)
	- exports
	- solution (US-domestic-airflights.ipynb)
	- environment.yml
	- readme.md

* The experiments repository contains experiments done before writing to our solution ipynb. Only for reference
* The exports repository contains insightful figures from the performed EDA and the model plots.
* The solution repository contains the US-domestic-airflights.ipynb and understand and predict delays in US domestic airlines
* To run the notebook, make sure to create an environment within anaconda with the following environment.yml file through the following command:
conda env create -f environment.yml
* Open the US-domestic-airflights.ipynb and run a cell by clicking a cell and pressing shift+enter or go to menu and click Cell - Run all.


# Some extra notes

## part 1: exploratory analysis

* Run using dask parallel processing library.
* Takes for the yearly analysis all available data from 1986 until 2008
* Takes for the rest of our analysis peristed data from 2008
* Saves to a parquet file 


## part 2: the model

* Takes a parquet file as input
* we will train our data on the first three weeks of january 2008 and carry out inference on the last week of january.
* We will use a stratified shuffle split to make sure there is balanced data ratio of 80/20 delay ratio in both train and test set.
* We will create a dummy simple classifier as baseline to compare against.
* We will then create a balanced logistic regression classifier to improve our model 
* We will do this in a sklearn pipeline, for reproducibility and executing same order of preprocesing on train and test set.
* By default the model runs a logistic_regression, if you would like a different model such as a random forrest, change get_pipeline() parameter to 'random_forrest'
* We standardscale numerical columns, we one-hot encode low and high cardinal categorical columns 
* We plot ROC curve, Precision-Recall curve and learning curve, print the classsification report with F1, precision recall, 

## Future work
Finally a lot of improvements are still do be explored. This is only the beginnning, a few possibilities are: 
* feature engineering, 
* Model optimization
* model parameters should not be left in te code but inside a params.json 
* missing values could be imputed with machine learning algorithms such MICE or KNN. ( to be checked )
* Better packaging, refactoring, testing, logging