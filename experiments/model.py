# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 12:00:49 2019

@author: zhangka
"""

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
#%matplotlib inline

import pandas as pd
import numpy as np

from dask.delayed import delayed
import dask.dataframe as dd
import dask

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, GridSearchCV, learning_curve
from sklearn.model_selection import cross_val_score, StratifiedShuffleSplit, StratifiedKFold, cross_val_predict, cross_validate

from sklearn.decomposition import PCA

from sklearn.base import BaseEstimator, TransformerMixin 
from pandas.api.types import CategoricalDtype

from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, precision_recall_curve
from sklearn.metrics import classification_report, confusion_matrix, f1_score, auc, average_precision_score
from sklearn.metrics import make_scorer, precision_score, recall_score


from sklearn.externals import joblib

from os.path import dirname, abspath, join, isdir 
import os 
import psutil
import gc
import time
import glob


# Define @delayed-function read flights
@delayed
def read_flights(filename, causes_columns):

    df = pd.read_csv(filename, 
                     dtype=dtypes, 
                     encoding="ISO-8859-1",
                     )#compression='bz2'
        
    # We will assume that a 0 delay means no delay, We will get the departure hour from
    [df[column].replace(0, np.nan, inplace=True) for column in causes_columns]
    df = df.assign(DepHour=df.CRSDepTime.clip(upper=2399)//100,
                   ArrHour=df.CRSArrTime.clip(upper=2399)//100,
                   DepDelayed=(df['DepDelay'].fillna(0)>15),
                   ArrDelayed=(df['ArrDelay'].fillna(0)>15))
    
    return df

# Define @delayed-function to read an informative file
@delayed
def read_info(filename, index):
    
    df = pd.read_csv(filename, index_col=index)#compression='bz2'
    
    return df


class ColumnsSelector(BaseEstimator, TransformerMixin):
    '''Select columns based on a given type
    Args:
        type: (string) type such as e.g: int64, str, 
    '''        
    
    def __init__(self, type):
        self.type = type

    def fit(self, X, y=None):
        return self

    def transform(self,X):
        return X.select_dtypes(include=[self.type])

class CategoricalImputer(BaseEstimator, TransformerMixin):
    '''For categorical columns imputation will be done by chooosing a strategy,
    such as fill most frequent. Since sklearn imputer only works for
    numerical values. Fit will create a dictionary for a category and transform
    will impute.
  
    Args:
        strategy: (string) Default is imputation by most_frequent, 
                 if not then 0 is imputed
                 columns: (list) Provide the list of columns with missing values 
    '''

    def __init__(self, columns = None, strategy='most_frequent'):
        self.columns = columns
        self.strategy = strategy
    
    def fit(self,X, y=None):
        if self.columns is None:
            self.columns = X.columns
            
        if self.strategy is 'most_frequent':
            self.fill = {column: X[column].value_counts().index[0] for 
                         column in self.columns}
        else:
            self.fill = {column: '0' for column in self.columns}
            
        return self
    
    def transform(self,X):
        X_copy = X.copy()
        for column in self.columns:
            X_copy[column] = X_copy[column].fillna(self.fill[column])
        
        return X_copy

class NumericalImputer(BaseEstimator, TransformerMixin):
    '''For categorical columns imputation will be done by chooosing a strategy,
    such as fill most frequent. Since sklearn imputer only works for
    numerical values. Fit will create a dictionary for a category and transform
    will impute.
  
    Args:
        strategy: (string) Default is imputation by median, 
                 if not then 0 is imputed
                 columns: (list) Provide the list of columns with missing values 
    '''

    def __init__(self, columns = None, strategy='median'):
        self.columns = columns
        self.strategy = strategy
    
    def fit(self,X, y=None):
        if self.columns is None:
            self.columns = X.columns
            
        if self.strategy is 'median':
            self.fill = {column: X[column].median() for  # BE SURE TO CHECK IF MEDIAN HOLDS ANY SENSE
                         column in self.columns}
        else:
            self.fill = {column: '0' for column in self.columns}
            
        return self
      

    def transform(self,X):
        X_copy = X.copy()
        for column in self.columns:
            X_copy[column] = X_copy[column].fillna(self.fill[column])
        
        return X_copy

class CategoricalEncoder(BaseEstimator, TransformerMixin):
    
    ''' For categorical columns an encoding strategy is needed. Here we will
    choose pd.get_dummies, which is a one-hot encoding based strategy.
    Since we need to try to fit all possible categories, we will concatenate
    our feature data. This to prevent that we would encounter unseen categories.
    For each category will we transform to a corresponding column name with
    category type values.
    
    Args:
         dropfirst: (boolean) True drops the first column, this to prevent 
         multicollinearity. False keeps the first column.
    
    '''
    def __init__(self, dropFirst=True):
        self.categories=dict()
        self.dropFirst=dropFirst

    def fit(self, X, y=None):
        train = X.copy() # TODO might need to add test data
        train = train.select_dtypes(include=['object'])
        for column in train.columns:
            self.categories[column] = train[column].value_counts().index.tolist()
            
        return self
    
    def transform(self, X):
        X_copy = X.copy()
        X_copy = X_copy.select_dtypes(include=['object'])
        for column in X_copy.columns:
            X_copy[column] = X_copy[column].astype({column:
                CategoricalDtype(self.categories[column])})
        
        return pd.get_dummies(X_copy, drop_first=self.dropFirst)


# Function to return the pipeline
def get_pipeline(model='logistic_regression'):
    # we need a pipeline for the numerical columns and the categorical columns.

#    pipeline_num = Pipeline([("num_selector", ColumnsSelector(type=np.number)),
#                             ("num_imputer", NumericalImputer(columns=missing_cols, strategy=0)),
#                             ("scaler", StandardScaler())])
    
    #missing_cols = ['Sex']
    pipeline_cat = Pipeline([#("cat_selector", ColumnsSelector(type='object')),
                             #("cat_imputer", CategoricalImputer(columns=missing_cols)),
                             ("encoder", CategoricalEncoder(dropFirst=True)),
                             ("PCA", PCA(n_components=100))
])

#    pipeline_processed = FeatureUnion([("pipeline_num", pipeline_num), 
#                ("pipeline_cat", pipeline_cat)])
    
    # Create a pipeline of transformers and estimator/
    pipeline_full = Pipeline([('pipeline_processed', pipeline_cat),
                              ('model',get_model_pipeline(model))])
    
    return pipeline_full, pipeline_cat

def get_model_pipeline(model='logistic_regression'):
    
    if model == 'logistic_regression':
        pipeline_model=LogisticRegression(C=1,
                                          penalty='l2',
                                          random_state=42,
                                          class_weight='balanced') 
        
    elif model == 'random_forrest':
        pipeline_model=RandomForestClassifier(n_estimators = 128, criterion = 'entropy', random_state=42)
    
    
    elif model == 'xgboost':
        pipeline_model=XGBClassifier()
    
    elif model == 'neural_net':
        pipeline_model=keras.wrappers.scikit_learn.KerasClassifier(build_fn=create_model,
                                                              batch_size=10,
                                                              epochs=50,
                                                              shuffle=True,
                                                              input_dim=22)

    return pipeline_model

def plot_PR(y_test, y_probs, precision, recall, auc_val, filename):

    
    fig = plt.figure()
    plt.title('Precision recall curve')
    plt.plot(recall, precision, 'b', label = "{0:.2f} %".format(100*auc_val))
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0.25, 0.25],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('precision')
    plt.xlabel('recall')
    #fig.savefig("exports/" + filename, bbox_inches='tight')

# Plot the confusion matrix and export as png file
def plot_CFM(y_test, y_pred, filename):
    fig = plt.figure()
    cfm = confusion_matrix(y_test, y_pred)
    sns.heatmap(cfm, annot=True)
    plt.xlabel('Predicted classes')
    plt.ylabel('Actual classes')
    #fig.savefig("exports/" + filename, bbox_inches='tight')

# Plot the ROC curve and export as png file
def plot_ROC(y_test, y_pred, roc_auc, filename):
    fpr, tpr, threshold = roc_curve(y_test.values, y_pred)
    
    fig = plt.figure()
    plt.title('Receiver Operating Characteristic')
    plt.plot(fpr, tpr, 'b', label = "{0:.2f} %".format(100*roc_auc))
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    #fig.savefig("exports/" + filename, bbox_inches='tight')

def plot_curve(clf,X, y, title):
    sizes = np.linspace(0.01, 0.1, 3)
    train_sizes,train_scores,test_scores = learning_curve(clf,X,y,scoring='roc_auc',random_state = 42,cv = 5, train_sizes=sizes)

    plt.figure()
    plt.title(title)
    
    ylim = (0.2, 1.01)
    if ylim is not None:
        plt.ylim(*ylim)
        
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                train_scores_mean + train_scores_std, alpha=0.1,
                color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
        label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
        label="Cross-validation score")

    plt.legend(loc="best")
    plt.show()


# We will first define our read path and the dtypes for 

read_path = abspath(dirname((os.getcwd())))
causes_columns = ['CarrierDelay','WeatherDelay', 'NASDelay', 'SecurityDelay', 'LateAircraftDelay']
dtypes = {
        'Year' : 'uint16',
        'Month' : 'uint8',
        'DayofMonth' : 'uint8',
        'DayOfWeek' : 'uint8',
        'DepTime' : 'float16',
        'CRSDepTime': 'float16',
        'ArrTime': 'float16',
        'CRSArrTime': 'float16',
        'UniqueCarrier' : 'str',
        'FlightNum' : 'int64',
        'TailNum' : 'str',
        'ActualElapsedTime' : 'float16',
        'CRSElapsedTime':'float16',
        'AirTime' : 'float64',
        'ArrDelay' : 'float32',
        'DepDelay' : 'float32',
        'Origin' : 'str',
        'Dest' : 'str',
        'Distance' : 'float64',
        'TaxiIn' : 'float64',
        'TaxiOut' : 'float64',
        'Cancelled' : 'bool',
        'CancellationCode' : 'str',
        'Diverted' : 'uint8',
        'CarrierDelay' : 'float32',
        'WeatherDelay' : 'float32',
        'NASDelay' : 'float32',
        'SecurityDelay' : 'float32',
        'LateAircraftDelay' : 'float32'
        }


# Get flight delays: flight_delays
filenames = glob.glob(os.path.join(read_path, "data", '[0-9]'*4+'.csv'))#bz2
    
dataframes = []
[dataframes.append(read_flights(filename, causes_columns)) for filename in filenames]

flight_delays = dd.from_delayed(dataframes)


# We will now work with the 2008 data
filenames = glob.glob(os.path.join(read_path, "data", '2008'+'.csv'))#bz2

dataframes = []
[dataframes.append(read_flights(filename, causes_columns)) for filename in filenames]

flight_delays = dd.from_delayed(dataframes)

#read airport
filename =  os.path.join(read_path, "data", 'airports'+'.csv')
airports = dd.from_delayed(read_info(filename, 'iata'))

#read carrier
filename =  os.path.join(read_path, "data", 'carriers'+'.csv')
carriers = dd.from_delayed(read_info(filename, 'Code'))

# persist the dataframe if fits in memory, works also in parallel
persist_flight_delays = flight_delays.persist()
#persist_flight_delays = persist_flight_delays.repartition(npartitions=4)# 1+flight_delays.memory_usage(deep=True).sum().compute()


# =============================================================================
# Start model 
# =============================================================================

#persist_flight_delays = flight_delays.repartition(npartitions=10)


use_cols = ['Year', 'Month', 'DayOfWeek', 'DepDelayed',
        'DepHour', 'UniqueCarrier', 'Origin', 'Dest'] #CRSArrTime, CRSElapsedTime, FlightNum, 
target = 'DepDelayed'


persist_flight_delays = persist_flight_delays[persist_flight_delays['Month']==1]

X = persist_flight_delays[['Month', 'DayOfWeek','DepHour', 'UniqueCarrier', 'Origin', 'Dest']].astype('object').compute() # 'Month', 'DayOfWeek', 'DepHour', 
#X = [persist_flight_delays[t] for t in use_cols if t !=target]
#X = [x for x in persist_flight_delays.columns if x in use_cols] 
y = persist_flight_delays[target].compute()


# Our target has an imbalanced class set.
y.value_counts()/len(y) # 25/75


# Lets check cardinality
for k in X.columns:
    print(k, X[k].cat.categories.shape[0])
    if X[k].unique().shape[0] < 50:
        print(X[k].unique())



#Stratified sampling will be necessary
stratified = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)

for train_index, test_index in stratified.split(X, y):
    X_training, X_val = X.loc[train_index], X.loc[test_index]
    y_training, y_val= y.loc[train_index], y.loc[test_index]




# Try random undersampling and oversampling
# Class count
#count_class_0, count_class_1 = y_training.value_counts()
#
##Divide by class
#df_train = pd.concat([X_training, y_training], axis=1)
#df_train_class_0 = df_train[df_train[target] == 0]
#df_train_class_1 = df_train[df_train[target] == 1]
#
#df_train_class_0_under = df_train_class_0.sample(count_class_1, replace=True)
#df_train_under = pd.concat([df_train_class_0_under, df_train_class_1], axis=0)
#
#print('Random under-sampling:')
#print(df_train_under[target].value_counts())
#
#X_training, y_training = df_train_under.drop([target], axis=1), df_train_under[target]
#
#


# We will start with some simple scatterplots against the target variable
# We don't need to plot against the own target variable
#use_cols2 = use_cols.copy()
#use_cols2.remove(target)
#
#columns_drop = list(set(persist_flight_delays.columns)-set(use_cols))
#df = persist_flight_delays.drop(columns_drop,axis=1)#.compute()
#

# =============================================================================
# 
# =============================================================================
from dask_ml.linear_model import LogisticRegression
#import dask.multiprocessing

# Using dask

begin = time.time()
X_ohe = dd.get_dummies(X.categorize(), drop_first=True) # .categorize()

lr = LogisticRegression()
#with dask.config.set(scheduler=dask.multiprocessing.get):
lr.fit(X_ohe.values, y.values)
end = time.time()
print(end-begin)

# Usind sklearn
from sklearn.linear_model import LogisticRegression
begin = time.time()
X_ohe = dd.get_dummies(X.categorize(), drop_first=True).compute() # .categorize()

lr = LogisticRegression()
lr.fit(X_ohe.values, y.values)
end = time.time()
print(end-begin)
# =============================================================================
# 
# =============================================================================




begin = time.time()
pipeline_full, pipeline_cat = get_pipeline('logistic_regression')
#pipeline_full.fit(X_training, y_training)
X_processed =  pipeline_cat.fit_transform(X_training)
end = time.time()
print(end-begin)


# For the evaluation set
y_val_pred = pipeline_full.predict(X_val)
y_val_probs = pipeline_full.predict_proba(X_val)
y_val_probs = y_val_probs[:,1] # predicting 0 or 1



# calculate precision-recall curve
precision, recall, thresholds = precision_recall_curve(y_val.values, y_val_probs)
# calculate precision-recall AUC
auc_val = auc(recall, precision)
ap = average_precision_score(y_val.values, y_val_probs)

# calculate F1 score
f1 = f1_score(y_val.values, y_val_pred)
print('f1=%.2f, auc=%.2f,  ap=%.2f' %(f1, auc_val, ap))


# predict probabilities
plot_PR(y_val.values, y_val_probs, precision, recall, auc_val,'Precision_recall_curve')

# Plot the confusion matrix and the ROC curve
plot_CFM(y_val.values, y_val_pred, 'baseline_lr_confusion_matrix.png')
   
roc_auc = roc_auc_score( y_val.values, y_val_probs) # Currently on a 0.68
plot_ROC(y_val, y_val_probs, roc_auc, 'baseline_lr_ROC.png')
#print("baseline AUC score: {0:.2f} %".format(100 * roc_auc))  

#Classification report including F1 score
print(classification_report(y_val.values,y_val_pred ))



# plot learning curves
plot_curve(LogisticRegression(), X_processed, y_training,'Learning Curve of Logistic Regression')




from dask_ml.linear_model import LogisticRegression
from dask_ml.compose import make_column_transformer
from dask_ml.preprocessing import StandardScaler, Categorizer, DummyEncoder
from dask_ml.wrappers import Incremental
from dask_ml.impute import SimpleImputer
from dask.diagnostics import ResourceProfiler, Profiler, ProgressBar


from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import FunctionTransformer
from sklearn.linear_model import SGDClassifier



# =============================================================================
# This works
# =============================================================================
from dask import dataframe as dd
from dask_glm.datasets import make_classification
from dask_ml.linear_model import LogisticRegression

X, y = make_classification(n_samples=10000, n_features=2)

X = dd.from_dask_array(X, columns=["a","b"])
X_ohe= dd.get_dummies(X.categorize(), drop_first=True)

y = dd.from_array(y)

lr = LogisticRegression()
lr.fit(X.values, y.values)