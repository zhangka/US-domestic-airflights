# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 08:49:11 2019

@author: zhangka
"""

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

from dask.delayed import delayed
import dask.dataframe as dd
import dask

from os.path import dirname, abspath, join, isdir 
import os 
import psutil
import gc
import time

import glob

read_path = abspath(dirname((os.getcwd()))) #get path
   # Read file
dtypes = {
        'Year' : 'uint16',
        'Month' : 'uint8',
        'DayofMonth' : 'uint8',
        'DayOfWeek' : 'uint8',
        'DepTime' : 'float16',
        'CRSDepTime': 'float16',
        'ArrTime': 'float16',
        'CRSArrTime': 'float16',
        'UniqueCarrier' : 'str',
        'FlightNum' : 'int64',
        'TailNum' : 'str',
        'ActualElapsedTime' : 'float16',
        'CRSElapsedTime':'float16',
        'AirTime' : 'float64',
        'ArrDelay' : 'float32',
        'DepDelay' : 'float32',
        'Origin' : 'str',
        'Dest' : 'str',
        'Distance' : 'float64',
        'TaxiIn' : 'float64',
        'TaxiOut' : 'float64',
        'Cancelled' : 'bool',
        'CancellationCode' : 'str',
        'Diverted' : 'uint8',
        'CarrierDelay' : 'float32',
        'WeatherDelay' : 'float32',
        'NASDelay' : 'float32',
        'SecurityDelay' : 'float32',
        'LateAircraftDelay' : 'float32'
        }
# Define @delayed-function read_flights
@delayed
def read_flights(filename):

    df = pd.read_csv(filename, 
                     dtype=dtypes, 
                     #parse_dates={"Date":[0,1,2]}, 
                     encoding="ISO-8859-1"
                     #blocksize=int(10e6)
                     )#compression='bz2'
    
    delay_columns = ['CarrierDelay','WeatherDelay', 'NASDelay', 'SecurityDelay', 'LateAircraftDelay']
    
    # We wwill assume that a 0 delay means no delay, We will get the departure hour
    [df[column].replace(0, np.nan, inplace=True) for column in delay_columns]
    df = df.assign(DepHour=df.CRSDepTime.clip(upper=2399)//100,
                   ArrHour=df.CRSArrTime.clip(upper=2399)//100,
                   DepDelayed=(df['DepDelay'].fillna(0)>15),
                   ArrDelayed=(df['ArrDelay'].fillna(0)>15))
    
    return df


# Define @delayed-function read airports
@delayed
def read_info(filename, index):
    
    df = pd.read_csv(filename, index_col=index)#compression='bz2'
    
    return df

# plot a barplot
def plot_bar(x, y, xlabel, ylabel, title):
    fig = plt.figure() 
    #figManager = plt.get_current_fig_manager()
    #figManager.window.showMaximized()
    g = sns.barplot(x=x, y=y, color='b')
    sns.despine()
    g.set(title=title, xlabel=xlabel, ylabel=ylabel)
    plt.xticks(rotation=45)
    fig.savefig(join(read_path, "exports",  title + '_bar.png'), bbox_inches='tight')

def plot_bar_melt(data, x, y, xlabel, title):
    fig = plt.figure() 
    g = sns.barplot(x =x, y=y, hue='variable', data=data)
    sns.despine()
    g.set(title=title, xlabel=xlabel)
    fig.savefig(join(read_path, "exports",  title + '_bar.png'), bbox_inches='tight')



# plot a barplot
def plot_scatter(x, y, xlabel, ylabel, title):
    fig = plt.figure() 
    sns.set(font_scale = 1)
    #sns.despine()
    g = sns.scatterplot(x=x, y=y,s=100)
    g.set(title=title, xlabel=xlabel, ylabel=ylabel)
    plt.xticks(rotation=45)

    #plt.ylabel(ylabel)
    #plt.xlabel(xlabel)
    #plt.show()
    fig.savefig(join(read_path, "exports",  title + '_scatter.png'), bbox_inches='tight')

def plot_heatmap(df, title):
    
    fig = plt.figure(1, figsize=(12,12))

    ax = fig.add_subplot(1,2,1)
    
    subset = df.iloc[:50, :]
    sns.heatmap(subset, linewidths=0.5, cmap="YlGnBu",vmin = 0, vmax = 50) #, mask=mask
    plt.setp(ax.get_xticklabels(), fontsize=12, rotation = 88) ;
    ax.yaxis.label.set_visible(False)
    
    ax = fig.add_subplot(1,2,2)    
    subset = df.iloc[50:100, :]
    fig.text(0.5, 1.02, title, ha='center', fontsize = 20)
    sns.heatmap(subset, linewidths=0.5, cmap="YlGnBu",vmin = 0, vmax = 50) #mask=mask, 
    plt.setp(ax.get_xticklabels(), fontsize=12, rotation = 88) ;
    ax.yaxis.label.set_visible(False)
    
    #plt.tight_layout()
    fig.savefig(join(read_path, "exports",  title + '.png'), bbox_inches='tight')

import matplotlib.patches as mpatches

def plot_piechart(df, title, carrier_dict):
    #font = {'family' : 'normal', 'weight' : 'bold', 'size'   : 15}
    #plt.rc('font', **font)

    colors = plt.cm.tab20.colors
    
    fig = plt.figure(1, figsize=(16,15))
    gs=plt.GridSpec(2,2)             
    ax=fig.add_subplot(gs[0,0]) 

    labels = [s for s in df.index]
    sizes  = df.values
    explode = [0.3 if sizes[i] < 150000 else 0.0 for i in range(len(df))]
    patches, texts, autotexts = ax.pie(sizes,explode=explode,
                                        colors=colors,
                                        labels=labels,  
                                        autopct='%1.0f%%',
                                        shadow=False, 
                                        startangle=0)
    [_.set_fontsize(14) for _ in texts]
    ax.axis('equal')
    ax.set_title(title,fontsize=18)

    comp_handler = []
    [comp_handler.append(mpatches.Patch(color=colors[i],
                label=df.index[i] + ': ' + carrier_dict[df.index[i]])) for i in range(len(df)-1)]
    plt.legend(handles=comp_handler, bbox_to_anchor=(0.2, 0.85), 
               fontsize=13, bbox_transform=plt.gcf().transFigure)
    
    fig.savefig(join(read_path, "exports",  title + '.png'), bbox_inches='tight')


if __name__ == '__main__':
    
    t_start = time.time()
    filenames = glob.glob(os.path.join(read_path, "data", '[0-9]'*4+'.csv'))#bz2
    
    dataframes = []
    for filename in filenames:
        dataframes.append(read_flights(filename))
    
    # Get flight delays: flight_delays
    flight_delays = dd.from_delayed(dataframes) #meta=dtypes
    #flight_delays.repartition(npartitions=24)# 1+flight_delays.memory_usage(deep=True).sum().compute() // 100MB
    
    
    # We will now work with the 2008 data
    filenames = glob.glob(os.path.join(read_path, "data", '2008'+'.csv'))#bz2
    
    dataframes = []
    for filename in filenames:
        dataframes.append(read_flights(filename))
    
    # Get flight delays: flight_delays
    flight_delays = dd.from_delayed(dataframes) #meta=dtypes
    
    #read airport
    filename =  os.path.join(read_path, "data", 'airports'+'.csv')
    airports = dd.from_delayed(read_info(filename, 'iata'))
    
    #read carrier
    filename =  os.path.join(read_path, "data", 'carriers'+'.csv')
    carriers = dd.from_delayed(read_info(filename, 'Code'))
    
    # persist the dataframe if fits in memory, works also in parallel
    persist_flight_delays = flight_delays.persist()
    
    
    # Print average of 'weather delay' column of flight_delays
   # print(persist_flight_delays['WeatherDelay'].mean().compute())
    #persist_flight_delays
    t_end = time.time()
    print('Time to calculate weather delay: {0:.2f} s'.format(t_end-t_start)) # To load all data is 663.74s
    
    
# =============================================================================
#     
# =============================================================================
    
    
    # Let's define some operations
    non_cancelled = persist_flight_delays[~persist_flight_delays.Cancelled]
    by_origin = non_cancelled.groupby('Origin')
    by_day = non_cancelled.groupby('DayOfWeek')

    
    # 1. Get airports with most delay and average delay
    t_start = time.time()

    most_delay_by_origin = by_origin['DepDelay'].sum()
    mean_delay_by_origin = by_origin['DepDelay'].mean()
    most_delay_by_origin, mean_delay_by_origin = dask.compute(most_delay_by_origin, 
                                                              mean_delay_by_origin)

    most_delay_by_airport = dd.merge(most_delay_by_origin.to_frame(), 
                                     airports, 
                                     left_index=True, 
                                     right_index=True)
    
    cols = list(most_delay_by_airport.columns[[0,2]])
    most_delay_by_airport_largest = most_delay_by_airport[cols] \
                                        .nlargest(10, columns='DepDelay') \
                                        .compute()
    print(most_delay_by_airport_largest)
    t_end = time.time()
    print('Time to calculate most/mean delay: {0:.2f} s'.format(t_end-t_start)) # To load all data is 663.74s

    # Take a look at the missing values: We ahe around 2% of missing dep delays and arrival delays. Which crresponds with elapsed tiomes.
    persist_flight_delays.isna().sum().compute()/len(persist_flight_delays)

#    #2. Amount of non-cancelled flights
#    t_start = time.time()    
#    #len(non_cancelled) # 14164761
#    t_end = time.time() # takes 195 sec
#    print('Time to calculate # flight delay: {0:.2f} s'.format(t_end-t_start)) # To load all data is 663.74s


#    # 3. Amount of non cancelled flights per airport
#    print(by_origin.Origin.count().compute())
#    
#    
#    # 4. Day of week with worst average delay: Friday
#    print(by_day['DepDelay'].mean().compute())

#    # Calculate main cause of delays.
#    cols = ['CarrierDelay', 'WeatherDelay', 'NASDelay', 'SecurityDelay', 'LateAircraftDelay']
#    delays = [persist_flight_delays[c].count().compute() for c in cols]
#    print('the delay for ' + cols + 'is' + delays)


    
# =============================================================================
#   Calculate some general flight info
# =============================================================================
    # Let's check the yearly change in US flights.
    
    
    # 1. Let's check some variables against the target variable.
    non_cancelled = persist_flight_delays[~persist_flight_delays.Cancelled]

    
    
    aggregations = (non_cancelled.groupby('Year').DepDelayed.mean(),
                    non_cancelled.groupby('Month').DepDelayed.mean(),
                    non_cancelled.groupby('Month').ArrDelayed.mean(),
                    non_cancelled.groupby('Month').DepDelayed.sum(),
                    non_cancelled.groupby('Month').ArrDelayed.sum(),
                    non_cancelled.groupby('Month').DepDelay.mean(),
                    non_cancelled.groupby('Month').ArrDelay.mean(),
                    
                    non_cancelled.groupby('Month').CarrierDelay.agg(['mean', 'count']),
                    non_cancelled.groupby('Month').WeatherDelay.agg(['mean', 'count']),
                    non_cancelled.groupby('Month').NASDelay.agg(['mean', 'count']),
                    non_cancelled.groupby('Month').SecurityDelay.agg(['mean', 'count']),
                    non_cancelled.groupby('Month').LateAircraftDelay.agg(['mean', 'count']),

                    non_cancelled.groupby('DayOfWeek').DepDelayed.mean(),                    
                    non_cancelled.groupby('DepHour').DepDelayed.mean(),
                    non_cancelled.groupby('AirTime').DepDelayed.mean(),
                    non_cancelled.groupby('Distance').DepDelayed.mean(),
                    non_cancelled.groupby('Origin').DepDelayed.mean().nlargest(15),
                    non_cancelled.groupby('Dest').DepDelayed.mean().nlargest(15),                    
                    non_cancelled.groupby('UniqueCarrier').DepDelayed.mean().nlargest(15),
                    non_cancelled.groupby('UniqueCarrier').DepDelayed.sum().nlargest(20),
                    flight_delays.groupby('UniqueCarrier').Cancelled.mean().nlargest(20),
                    flight_delays.groupby('UniqueCarrier').Diverted.mean().nlargest(20),

                    
                    non_cancelled.groupby('Origin').DepDelay.sum().nlargest(100),
                    non_cancelled.groupby('Origin').DepDelay.mean(),

        
                    flight_delays.groupby('Cancelled').count()
                    )
    

    (avg_depdelayed_by_year,
     avg_depdelayed_by_month,avg_arrdelayed_by_month, 
     count_depdelayed_by_month,count_arrdelayed_by_month, 
     avg_depdelay_by_month,avg_arrdelay_by_month,    
     CarrierDelay_by_month,
     WeatherDelay_by_month,
     NASDelay_by_month,SecurityDelay_by_month,
     LateAircraftDelay_by_month,
     
     avg_depdelayed_by_day,
     avg_depdelayed_by_hour,avg_depdelayed_by_airtime,
     avg_depdelayed_by_distance, avg_depdelayed_by_origin,
     avg_depdelayed_by_dest, 
     avg_depdelayed_by_carrier,most_depdelayed_by_carrier,
     most_cancelled_by_carrier,most_diverted_by_carrier,

     most_depdelayed_by_origin, mean_depdelayed_by_origin,
     count_cancelled ) = dask.compute(*aggregations)
    
    
    # Merge the airport names to the result.
    most_depdelayed_by_airport = dd.merge(
            most_depdelayed_by_origin.to_frame(), 
            airports, 
            left_index=True, 
            right_index=True
            ).compute().sort_values('DepDelay', ascending=False)
    
    # Merge the airport names to the result.
    avg_depdelayed_by_airport_dest = dd.merge(
            avg_depdelayed_by_dest.to_frame(), 
            airports, 
            left_index=True, 
            right_index=True
            ).compute().sort_values('DepDelayed', ascending=False)
    
    # Merge the carrier names to the result
    avg_depdelayed_by_carriername = dd.merge(
            avg_depdelayed_by_carrier.to_frame(), 
            carriers, 
            left_index=True, 
            right_index=True
            ).compute().sort_values('DepDelayed', ascending=False)

    # 1. Plot most delay time for the most frequent origin airports
    plot_bar(most_depdelayed_by_airport['airport'], 
             most_depdelayed_by_airport['DepDelay'],
             'airports','Departure delay','cumulative departure delays per airport2')
    
    # 2. Plot average delays per destination
    plot_bar(avg_depdelayed_by_airport_dest['airport'], 
             avg_depdelayed_by_airport_dest['DepDelayed'],
             'dest','Delay fraction', 'departure delay per airport')   
    
    # 3. Plot average delays per carrier
    plot_bar(avg_depdelayed_by_carriername['Description'], 
             avg_depdelayed_by_carriername['DepDelayed'],
             'carrier','Delay fraction', 'delay_fraction_per_carrier')  
    
    # 4. Plot average delays per year
    plot_scatter(avg_depdelayed_by_year.index, avg_depdelayed_by_year.values,
             'year','Delay fraction', 'delay_fraction_by_year')
    
    # 5. Plot number of delayed flights per month
    count_delayed_by_month = pd.concat([count_depdelayed_by_month, count_arrdelayed_by_month], axis=1)
    count_delayed_by_month.rename(index=month, inplace=True)
    count_delayed_by_month = pd.melt(count_delayed_by_month.reset_index(), id_vars='Month')
    plot_bar_melt(count_delayed_by_month ,'Month', 'value',"delays per month", "count_delayed_by_month")

    
    # 6. Plot average amount of delays per month
    month = {1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May',
            6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec'}
    
    avg_delayed_by_month = pd.concat([avg_depdelayed_by_month, avg_arrdelayed_by_month], axis=1)
    avg_delayed_by_month.rename(index=month, inplace=True)
    avg_delayed_by_month = pd.melt(avg_delayed_by_month.reset_index(), id_vars='Month')
    plot_bar_melt(avg_delayed_by_month ,'Month', 'value',"average delayed per month ", "avg_delayed_by_month")

    
    # 7. Plot average delay duration per month, this shows that on average the departure delay is heavier than the arrival delay.    
    avg_delay_by_month = pd.concat([avg_depdelay_by_month, avg_arrdelay_by_month], axis=1)
    avg_delay_by_month.rename(index=month, inplace=True)
    avg_delay_by_month = pd.melt(avg_delay_by_month.reset_index(), id_vars='Month')

    plot_bar_melt(avg_delay_by_month ,'Month', 'value',"delay duration per month [min]", "avg_delay_by_month")

    
    # 8. Plot average delays per day
    day = {1: 'Mon', 2: 'Tue', 3: 'Wed', 4: 'Thu', 5: 'Fri', 6: 'Sat', 7: 'Sun'}
    avg_depdelayed_by_day.index = avg_depdelayed_by_day.index.map(day)
    plot_bar(avg_depdelayed_by_day.index,avg_depdelayed_by_day.values ,
             'day','Departure delay', 'delay_fraction_per_day')
    
    # 9. Plot average delays per hour
    plot_bar(avg_depdelayed_by_hour.index, avg_depdelayed_by_hour.values,
             'hour','Delay fraction', 'delay_fraction_per_hour')    

    # 10. Plot delay causes --> This is usually registererd when the delay is > 15min at arrival.
    causes_columns = ['CarrierDelay', 'WeatherDelay','NASDelay', 'SecurityDelay', 'LateAircraftDelay']
    causes = non_cancelled.dropna(how='all',subset=causes_columns)
    
    to_mean = causes.groupby('Month')[causes_columns].mean().compute()
    to_mean.rename(index=month, inplace=True)
    to_mean = pd.melt(to_mean.reset_index(), id_vars='Month')    
    plot_bar_melt(to_mean ,'Month', 'value',"Cause of delay", "avg_cause_by_month")
    
    # 11. Plot delay causes
    to_count = causes.groupby('Month')[causes_columns].count().compute()
    to_count.rename(index=month, inplace=True)
    to_count = pd.melt(to_count.reset_index(), id_vars='Month')
    plot_bar_melt(to_count ,'Month', 'value',"Cause of delay", "cause_by_month_count")

    # 11. Plot delay causes rates per month
    cause_by_month_rate = to_mean/to_count   
    cause_by_month_rate = pd.melt(cause_by_month_rate.reset_index(), id_vars='Month')
    plot_bar_melt(cause_by_month_rate ,'Month', 'value',"Cause of delay", "avg_cause_by_month_rate")

# =============================================================================
# Let's compare Airline carriers
# =============================================================================
    
    # Let's get the
    carrier_dict = carriers['Description'].compute().to_dict()
    airport_dict = airports['airport'].compute().to_dict()
    carrier_dict['HP'] = 'US Airways'
    carrier_dict['US'] = 'US Airways'

    # 12. Let' s check the amount of flights per carrier in a pie chart
    to = non_cancelled.groupby('UniqueCarrier')['ArrTime'].count().compute().sort_values(ascending=False)
    plot_piechart(to, '% of flights per carrier',carrier_dict )

    
    # 13. Plot delays from origin airport, we notice that most of the carriers don't exceed too much delay.However some airlines do have some bad delays.
    # Most delays seem to come from outhwest airlines, however their delays never are very large. Pinacle and US airlines have much worse delays.
    to = non_cancelled.groupby(['Origin', 'UniqueCarrier'])['DepDelay'].mean().compute().unstack()
    to = to.loc[most_depdelayed_by_origin.index, most_depdelayed_by_carrier.index].dropna(axis=1, thresh=1)
    
    to.rename(columns=carrier_dict, index=airport_dict, inplace=True)    
    plot_heatmap(to, "Scale of Delays from origin airport")
    
    
    # 14. Let's check the % amount of cancellation and divertions
    most_cancelled_by_carrier.rename(index=carrier_dict, inplace=True)
    plot_bar(most_cancelled_by_carrier.index, most_cancelled_by_carrier.values,
         'carrier','cancelled', 'cancellations_by_carrier')

    most_diverted_by_carrier.rename(index=carrier_dict, inplace=True)
    plot_bar(most_diverted_by_carrier.index, most_diverted_by_carrier.values,
         'carrier','diverted', 'diverted_by_carrier')

    # 15. Now let's plot per carrier the cause of delays. This is only available for delayed flights (>15min) and not always recorded
    causes = non_cancelled.dropna(how='all',subset=causes_columns)
    to = causes.groupby('UniqueCarrier')[causes_columns].mean().compute()
    to.rename(index=carrier_dict, inplace=True)
    to = pd.melt(to.reset_index(), id_vars='UniqueCarrier')
    plot_bar_melt(to ,'value', 'UniqueCarrier',"Cause of delay", "avg_cause_by_carrier")

    # 16. Let's take a look at the taxi in and taxi out time. It is clear that the outgoing taxi times are much higher 
    to = non_cancelled.groupby('UniqueCarrier')['TaxiIn','TaxiOut'].mean().compute()
    to.rename(index=carrier_dict, inplace=True)
    to = pd.melt(to.reset_index(), id_vars='UniqueCarrier')
    plot_bar_melt(to ,'value', 'UniqueCarrier',"taxi_time [min]", "avg_Taxi_in_out_by_carrier")

    # 17. Airline speed is around 400mph, but far outliers are possible.
    to = non_cancelled.groupby('UniqueCarrier')['Distance','AirTime'].mean().compute()
    to = to[~to.isin([np.nan, np.inf, -np.inf]).any(1)]
    to["Speed"] = (to["Distance"]/to["AirTime"]*60)
    to = to.rename(index=carrier_dict).reset_index()
    plot_bar(to['Speed'], to['UniqueCarrier'], 'speed', 'carrier','speed per carrier')


    # 18. arrival and departure delays: Usually departure delays are longer than arrival delays, but this does not always seem to be the case.
    to = non_cancelled.groupby('UniqueCarrier')['ArrDelay','DepDelay'].mean().compute()
    to.rename(index=carrier_dict, inplace=True)
    to = pd.melt(to.reset_index(), id_vars='UniqueCarrier')
    plot_bar_melt(to ,'value', 'UniqueCarrier', "Mean Delay [min]", "avg_ArrDepDelay_by_carrier")



    # Save file to parquet
    #flight_delays.to_parquet(os.path.join(read_path, "data", 'flight-delays-parquet'))